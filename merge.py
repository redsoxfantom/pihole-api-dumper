import os
import argparse
import datetime
import json
from pathlib import Path

def compare_queries(q1,q2):
    return q1['timestamp'] == q2['timestamp'] and \
            q1['type'] == q2['type'] and \
            q1['subdomain'] == q2['subdomain'] and \
            q1['domain'] == q2['domain'] and \
            q1['tld'] == q2['tld'] and \
            q1['fld'] == q2['fld'] and \
            q1['result'] == q2['result']

parser = argparse.ArgumentParser(description="Merge 2 api dump files into one, dropping duplicates")
parser.add_argument('--f1', required=True, help="First file to merge")
parser.add_argument('--f2', required=True, help="Second file to merge")
args = parser.parse_args()

first = args.f1
second = args.f2
merged_file = datetime.datetime.utcnow().replace(microsecond=0).isoformat().replace(":","-") + "Z.merged.json"

print(F"Merging {first} with {second}, output file will be {merged_file}")

with open(first) as f1:
    firstcontents = json.load(f1)
with open(second) as f2:
    secondcontents = json.load(f2)

def iterate_over_contents(c1,c2,dropdups):
    dup_count = 0
    non_dups = []
    for query in c1:
        for secondqueryidx in range(len(c2)):
            secondquery = c2[secondqueryidx]
            if compare_queries(query,secondquery):
                if dropdups:
                    dup_count = dup_count + 1
                    break
                else:
                    non_dups.append(query)
                    break
            if secondquery['timestamp'] > query['timestamp']:
                non_dups.append(query)
                break
    return dup_count,non_dups

dup_count = 0
non_dups = []
dc1,nd1 = iterate_over_contents(firstcontents,secondcontents,True)
dup_count += dc1
non_dups += nd1
dc2,nd2 = iterate_over_contents(secondcontents,firstcontents,False)
dup_count += dc2
non_dups += nd2

with open(merged_file, 'w') as f:
    json.dump(non_dups, f)

print(F"Done, found {len(non_dups)} non duplicate queries")