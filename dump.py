import pihole as ph
import os
import argparse
import datetime
import json
from utils import arr_to_query_dict


parser = argparse.ArgumentParser(description= "Dump the last day's worth of pihole data to a json file")
parser.add_argument('--ip', help= "IP address of the pihole instance to dump", required=False)
parser.add_argument('--pw', help= "Admin password of the pihole instance", required=False)
args = parser.parse_args()

pihole_ip = args.ip
if not pihole_ip:
    pihole_ip = os.environ['PIHOLE_IP_ADDRESS']

pihole_password = args.pw
if not pihole_password:
    pihole_password = os.environ['PIHOLE_PASSWORD']

print(F"Connecting to pihole instance at {pihole_ip}...")
pihole = ph.PiHole(pihole_ip)
pihole.authenticate(pihole_password)
pihole.refresh()
pihole_version = pihole.getVersion()['core_current']
dump_file = datetime.datetime.utcnow().replace(microsecond=0).isoformat().replace(":","-") + "Z.json"

print(F"Successfully connected to pihole {pihole_version}. Dumping last day's worth of data to {dump_file}")
pihole_allqueries = pihole.getAllQueries(date_from="yesterday")
# Go through the queries structure and replace the queryactiontypes
modified_allqueries = []
valid_query_responses = set(resp.value for resp in ph.QueryActionType)
valid_query_types = set(["A (IPv4)", "HTTPS"])
for query in pihole_allqueries:
    query_resp = query[4]
    query_type = query[1]
    if query_resp in valid_query_responses and query_type in valid_query_types:
        if query_resp == ph.QueryActionType.CACHE.value or query_resp == ph.QueryActionType.FORWARDED.value:
            query_resp = "ALLOWED"
        else:
            query_resp = "BLOCKED"
        query[4] = query_resp
        query_dict = arr_to_query_dict(query)
        modified_allqueries.append(query_dict)

sorted_modified_queries = sorted(modified_allqueries, key=lambda d: d['timestamp'])
with open(dump_file, 'w') as f:
    json.dump(sorted_modified_queries, f)

print(F"Done, processed {len(modified_allqueries)} queries")