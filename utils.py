from tld import get_tld

def arr_to_query_dict(queryarr):
    dict = {}
    dict["timestamp"] = queryarr[0]
    dict["type"] = queryarr[1]
    parsed_domain = get_tld(queryarr[2], as_object=True,fix_protocol=True)
    dict['subdomain'] = parsed_domain.subdomain
    dict['domain'] = parsed_domain.domain
    dict['tld'] = parsed_domain.tld
    dict['fld'] = parsed_domain.fld
    dict["result"] = queryarr[4]
    return dict
