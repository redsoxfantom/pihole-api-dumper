import numpy as np
import pandas as pd
import tensorflow as tf

model = tf.keras.models.Sequential([
    tf.keras.layers.Dense(units=5, activation='relu'),
    tf.keras.layers.Dense(units=256, activation='relu'),
    tf.keras.layers.Dense(units=1, activation='sigmoid')
])

train_allowed = pd.read_csv("OUTPUT/allowed/2021-11-19T19-47-37Z.csv")
train_allowed["Result"] = "ALLOWED"
train_blocked = pd.read_csv("OUTPUT/blocked/2021-11-19T19-47-37Z.csv")
train_blocked["Result"] = "ALLOWED"
training_data = pd.concat([train_allowed, train_blocked])
training_data = training_data.sample(frac=1)
training_labels = training_data.pop("Result")
training_data = np.array(training_data)
training_labels = np.array(training_labels)

model.compile(loss='binary_crossentropy', metrics=['acc'], optimizer='sgd')

model.fit(training_data, training_labels, epochs=2)