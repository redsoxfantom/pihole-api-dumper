import json
import json_stream
import os
import argparse

parser = argparse.ArgumentParser(description="Convert a dump .json file to a pair of CSVs for training")
parser.add_argument("--file", type=str, help="Name of the json file to convert")
parser.add_argument("--output", type=str, help="Folder to put the converted CSVs in. Will be created if it doesn't exist")
args = parser.parse_args()

inFile = args.file
outFileName = os.path.splitext(os.path.basename(inFile))[0] + ".csv"
outFolder = args.output
blockedFolder = os.path.join(outFolder, "blocked")
blockedFile = os.path.join(blockedFolder, outFileName)
allowedFolder = os.path.join(outFolder, "allowed")
allowedFile = os.path.join(allowedFolder, outFileName)

if not os.path.exists(outFolder):
    os.makedirs(outFolder)
if not os.path.exists(blockedFolder):
    os.mkdir(blockedFolder)
if not os.path.exists(allowedFolder):
    os.mkdir(allowedFolder)

if not os.path.exists(inFile):
    raise IOError(F"Cannot parse {inFile} because it doesn't exist!")

with open(inFile, "r") as input, open(blockedFile, "w") as blocked, open(allowedFile, "w") as allowed:
    blocked.write("Type,Subdomain,Domain,Tld,Fld\n")
    allowed.write("Type,Subdomain,Domain,Tld,Fld\n")
    queries = json_stream.load(input)
    for query in queries:
        type = query['type']
        subdomain = query['subdomain']
        domain = query['domain']
        tld = query['tld']
        fld = query['fld']
        if query["result"] == "ALLOWED":
            fileToWriteTo = allowed
        else:
            fileToWriteTo = blocked
        fileToWriteTo.write(F"{type},{subdomain},{domain},{tld},{fld}\n")

print(F"Done, allowed file is at {allowedFile}, blocked file is at {blockedFile}")